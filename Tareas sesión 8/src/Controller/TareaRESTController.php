<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use App\Entity\Tarea;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/tareas/api")
 */
class tareaRESTController extends FOSRestController
{
    /**
     * @Rest\Get("/", name="lista_tareas")
     */
    public function lista_tareas()
    {
        $serializer = $this->get('jms_serializer');
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repositorio->findAll();
        if (count($tareas) > 0)
        {
            $respuesta = [
                'ok' => true,
                'tareas' => $tareas
            ];    
        } else {
            $respuesta = [
                'ok' => false,
                'error' => 'No se han encontrado tareas'
            ];        
        }
        return new Response($serializer->serialize($respuesta, "json"));
    }

    /**
     * @Rest\Get("/{id}", name="busca_tarea")
     */
    public function busca_tarea($id)
    {
        $serializer = $this->get('jms_serializer');
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tarea = $repositorio->find($id);
        if ($tarea)
        {
            $respuesta = [
                'ok' => true,
                'tarea' => $tarea
            ];    
        } else {
            $respuesta = [
                'ok' => false,
                'error' => 'tarea no encontrada'
            ];        
        }
        return new Response($serializer->serialize($respuesta, "json"));
    }   

    /**
     * @Rest\Post("/", name="nueva_tarea")
     */
    public function nueva_tarea(Request $request, ValidatorInterface $validator)
    {
        $serializer = $this->get('jms_serializer');
        $tarea = new tarea();
        $tarea->setTitulo($request->get('titulo'));
        $tarea->setAnyo($request->get('anyo'));

        $errores = $validator->validate($tarea);

        if (count($errores) == 0)
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tarea);
            $entityManager->flush();    
    
            $respuesta = [
                'ok' => true,
                'tarea' => $tarea
            ];        
        } else {
            $respuesta = [
                'ok' => false,
                'error' => 'Los datos no son correctos'
            ];        
        }

        return new Response($serializer->serialize($respuesta, "json"));
    }

    /**
     * @Rest\Delete("/{id}", name="borra_tarea")
     */
    public function borra_tarea($id)
    {
        $serializer = $this->get('jms_serializer');
        $entityManager = $this->getDoctrine()->getManager();
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tarea = $repositorio->find($id);
        if ($tarea)
        {
            $entityManager->remove($tarea);
            $entityManager->flush();
            $respuesta = [
                'ok' => true,
                'tarea' => $tarea
            ];    
        } else {
            $respuesta = [
                'ok' => false,
                'error' => 'tarea no encontrada'
            ];        
        }
        return new Response($serializer->serialize($respuesta, "json"));
    }   

    /**
     * @Rest\Put("/{id}", name="modifica_tarea")
     */
    public function modifica_tarea($id, Request $request)
    {
        $serializer = $this->get('jms_serializer');
        $entityManager = $this->getDoctrine()->getManager();
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tarea = $repositorio->find($id);
        if ($tarea)
        {
            $tarea->setTitulo($request->get('titulo'));
            $tarea->setAnyo($request->get('anyo'));
            $entityManager->flush();
            $respuesta = [
                'ok' => true,
                'tarea' => $tarea
            ];    
        } else {
            $respuesta = [
                'ok' => false,
                'error' => 'tarea no encontrada'
            ];        
        }
        return new Response($serializer->serialize($respuesta, "json"));
    }   
}


?>

