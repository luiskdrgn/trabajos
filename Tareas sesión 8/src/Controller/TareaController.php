<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Tarea;
use Jenssegers\Date\Date;

class TareaController extends AbstractController
{
    /**
     * @Route("/tareas", name="lista_tareas")
     */
    public function lista_tareas()
    {
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repositorio->findAll();

        if (count($tareas) > 0)
        {
            $resultado = "";
            foreach($tareas as $tarea)
                $resultado .= $tarea->getDescripcion() . " (" . 
                              $tarea->getFecha() . ")<br />";
            return new Response($resultado);
        } else {
            return new Response("No se han encontrado tareas");
        }
    }

    /**
     * @Route("/fechas", name="fechas")
     */
    public function fechas()
    {
        Date::setLocale('es');
        $hoy = Date::now();
        $nacimiento = Date::createFromFormat('d/m/Y', '7/4/1978');
        $diferencia = $nacimiento->timespan();

        return new Response('Hoy es ' . $hoy->format('d/m/Y') . '<br />' .
            'Nacimiento el ' . $nacimiento->format('d/m/y') . '<br />' .
            'Han pasado: ' . $diferencia);
    }
}


?>