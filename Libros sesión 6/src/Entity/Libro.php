<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LibroRepository")
 */
class Libro
{
    /**
     * @ORM\Column(type="string", length=20)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=100)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private $autor;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $paginas;

    public function getIsbn()
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getAutor(): ?string
    {
        return $this->autor;
    }

    public function setAutor(string $autor): self
    {
        $this->autor = $autor;

        return $this;
    }

    public function getPaginas(): ?string
    {
        return $this->paginas;
    }

    public function setPaginas(int $paginas): self
    {
        $this->paginas = $paginas;

        return $this;
    }
}
