<?php

namespace App\Service;

class BDPrueba
{
    private $libros = array(
        array("codigo" => 1, "titulo" => "Canción de hielo y fuego", "paginas" => 345, "autor" => "Luis Fernández"),
        array("codigo" => 2, "titulo" => "Pepita y Luis Adventures", "paginas" => 543, "autor" => "Luis Fernández"),
        array("codigo" => 3, "titulo" => "Teo busca esposa", "paginas" => 346, "autor" => "Luis Fernández"),
        array("codigo" => 4, "titulo" => "IT", "paginas" => 678, "autor" => "Luis Fernández"),
        array("codigo" => 5, "titulo" => "El niño con el traje de círculos", "paginas" => 345, "autor" => "Luis Fernández"),
    );    

    public function get()
    {
        return $this->libros;
    }
}

?>