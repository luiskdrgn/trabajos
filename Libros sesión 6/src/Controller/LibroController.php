<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class libroController extends AbstractController
{
    private $libros = array(
        array("codigo" => 1, "titulo" => "Canción de hielo y fuego", "paginas" => 345, "autor" => "Luis Fernández"),
        array("codigo" => 2, "titulo" => "Pepita y Luis Adventures", "paginas" => 543, "autor" => "Luis Fernández"),
        array("codigo" => 3, "titulo" => "Teo busca esposa", "paginas" => 346, "autor" => "Luis Fernández"),
        array("codigo" => 4, "titulo" => "IT", "paginas" => 678, "autor" => "Luis Fernández"),
        array("codigo" => 5, "titulo" => "El niño con el traje de círculos", "paginas" => 345, "autor" => "Luis Fernández"),
    );    

     /**
      * @Route("/libro/{codigo}", name="ficha_libro", requirements={"codigo"="\d+"});
      */
    public function ficha($codigo)
    {
        $resultado = array_filter($this->libros, function($libro) use ($codigo)
        {
            return $libro["codigo"] == $codigo;
        });

        if (count($resultado) > 0)
            return $this->render('ficha_libro.html.twig', array(
                'libro' => array_shift($resultado)
            ));
        else
            return $this->render('ficha_libro.html.twig', array(
                'libro' => NULL
            ));
    }

    /**
     * @Route("/libro/{texto}", name="buscar_libro")
     */
    public function buscar($texto)
    {
        $resultado = array_filter($this->libros, function($libro) use ($texto)
        {
            return strpos($libro["titulo"], $texto) !== FALSE;
        });

        return $this->render('lista_libros.html.twig', array(
            'libros' => $resultado
        ));
    }

    /**
     * @Route("/libro/insertar", name="insertar_libro")
     */
    public function insertar()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $libro = new Libro();
        $libro->setIsbn("1111AAAA");
        $libro->setTitulo("Libro de prueba");
        $libro->setAutor("Autor de prueba");
        $libro->setPaginas(100);

        return new Response("Libro insertado con id " . $libro->getId());
    }

    public function insertarConEditorial()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $editorial = new Editorial();
        $editorial->setNombre("Alfaguara");
        
        $libro = new Libro();
        $libro->setNombre("Inserción de prueba con editorial");
        $libro->setTelefono("900220022");
        $libro->setEmail("insercion.de.prueba.editorial@libro.es");
        $libro->setEditorial($editorial);

        $entityManager->persist($editorial);
        $entityManager->persist($libro);
        $entityManager->flush();

        return new Response("Libro insertado con id " . $libro->getId());
    }

}



?>