<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Editorial;


class LibroType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isbn', HiddenType::class)
            ->add('titulo', TextType::class)
            ->add('autor', TextType::class)
            ->add('paginas', TextType::class, array('label' => 'Correo electrónico'))
            ->add('editorial', EntityType::class, array(
                'class' => Editorial::class,
                'choice_label' => 'titulo',
              ))
            ->add('save', SubmitType::class, array('label' => 'Enviar'));
    }
}

?>