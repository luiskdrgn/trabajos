<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 13/02/19
 * Time: 19:25
 */

namespace ProyectoWeb\app\controllers;


use ProyectoWeb\exceptions\NotFoundException;
use ProyectoWeb\repository\CategoryRepository;
use ProyectoWeb\repository\ProductRepository;
use Psr\Container\ContainerInterface;

class productController
{
    protected $container;

    /**
     * productController constructor.
     * @param $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function ficha($request, $response, $args)
    {
        extract($args);

        $repositorio = new ProductRepository();
        $repositorioCateg = new CategoryRepository();
        try{
            $producto = $repositorio->findById($id);
            $categorias = $repositorioCateg->findAll();

        }catch(NotFoundException $nfe){
            return $response->write("Producto no encontrado");
        }

        $title = $producto->getNombre();
        $relacionados = $repositorio->getRelacionados($producto->getId(), $producto->getIdCategoria());

        return $this->container->renderer->render($response, "product.view.php", compact('title', 'categorias', 'producto', 'relacionados'));
    }


}